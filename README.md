Disflux
=======

A Discord bot designed to push live activity metrics to an InfluxDB database - to be graphed by, for example,
Grafana.

Requirements
------------

We only support running Disflux as a Docker container, and we provide an automatically-built Docker image at 
`gdude2002/Disflux`. If you're using Docker-Compose (and you should be), you'll find an example `docker-compose.yml`
in the `docker/` directory.

Configuration
-------------

Configuration is entirely handled using environment variables. As this bot does not store any data on its own, you're
quite safe to avoid mounting any volumes.

### Required

* `BOT_TOKEN`: The Discord bot token to connect with

### Optional

* `INFLUX_DATABASE`: Which InfluxDB database to connect to - defaults to `discord`
* `INFLUX_HOST`: The hostname of the InfluxDB instance to use - defaults to `localhost`
* `INFLUX_PASSWORD`: The InfluxDB password to authorize with - defaults to an empty password
* `INFLUX_PORT`: The port of the InfluxDB instance to use - defaults to `8086`
* `INFLUX_USERNAME`: The InfluxDB username to authorize with - defaults to an empty username

Usage
-----

Once you've set up your `docker-compose.yml` with the correct settings, simply `docker-compose up -d` to start the
bot in the background. Once the bot has been started, feel free to add it to the servers you'd like to monitor in
the usual way. The bot will start pushing metrics immediately.

Please note - The bot will not request previous data from Discord, and it will not attempt to catch up on messages
or other data that it missed if you take it offline for a while.
