import os


class Config:
    def __init__(self):
        if not self.bot_token:
            raise ValueError("Must supply the BOT_TOKEN environment variable.")


    @property
    def bot_token(self):
        return os.getenv("BOT_TOKEN", None)

    @property
    def influx_database(self):
        return os.getenv("INFLUX_DATABASE", "discord")

    @property
    def influx_host(self):
        return os.getenv("INFLUX_HOST", "localhost")

    @property
    def influx_password(self):
        return os.getenv("INFLUX_PASSWORD", None)

    @property
    def influx_port(self):
        return int(os.getenv("INFLUX_PORT", 8086))

    @property
    def influx_username(self):
        return os.getenv("INFLUX_USERNAME", None)
