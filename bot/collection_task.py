import asyncio
import logging
import typing
from collections import defaultdict
from contextlib import suppress
from datetime import datetime

from discord import Guild, Status, Member, CategoryChannel, VoiceChannel, Game, Streaming, Spotify
from discord.ext.commands import Bot

if typing.TYPE_CHECKING:
    from bot.cogs.metrics import Metrics


log = logging.getLogger(__name__)


class PeriodicStatsCollector:
    def __init__(self, bot: Bot, cog: "Metrics", delay: int):
        self.bot = bot
        self.cog = cog
        self.delay = delay

        self.running = False
        self._task = None

    async def start(self):
        if not self.running:
            self.running = True
            self._task = asyncio.ensure_future(self._run())
            log.info("Collection task started")

    async def stop(self):
        if self.running:
            self.running = False
            self._task.cancel()

            with suppress(asyncio.CancelledError):
                await self._task

            log.info("Collection task stopped")

    async def _run(self):
        while True:
            await asyncio.sleep(self.delay)

            if self.bot.is_closed():
                await self.stop()
                break

            try:
                now = datetime.utcnow()

                for guild in self.bot.guilds:  # type: Guild
                    channels = guild.channels
                    members = guild.members

                    bots = 0

                    dnd = 0
                    idle = 0
                    online = 0

                    offline = 0

                    activities = defaultdict(lambda: 0)
                    spotify_points = []

                    for member in members:  # type: Member
                        if member.status == Status.do_not_disturb:
                            dnd += 1

                        elif member.status == Status.idle:
                            idle += 1

                        elif member.status == Status.online or member.status == Status.invisible:
                            online += 1

                        else:
                            offline += 1

                        if member.bot:
                            bots += 1

                        activity = member.activity

                        if activity:
                            if isinstance(activity, Game):
                                activities["game"] += 1

                                activities[f"game_name_{activity.name}"] += 1

                            elif isinstance(activity, Streaming):
                                activities["streaming"] += 1

                                activities[f"streaming_name_{activity.name}"] += 1
                                activities[f"streaming_url_{activity.url}"] += 1

                            elif isinstance(activity, Spotify):
                                activities["spotify"] += 1

                                activities[f"spotify_album_{activity.album}"] += 1
                                activities[f"spotify_artist_{activity.artist}"] += 1
                                activities[f"spotify_title_{activity.title}"] += 1

                                spotify_data = {
                                    "artist": activity.artist,
                                    "album": activity.album,
                                    "title": activity.title,

                                    "guild_id": guild.id,
                                    "member_id": member.id,
                                    "track_id": activity.track_id
                                }

                                spotify_points.append(
                                    {
                                        "fields": spotify_data,
                                        "tags": {k: str(v) for k, v in spotify_data.items()},

                                        "time": activity.start
                                    }
                                )

                            else:
                                activities["other"] += 1

                                activities[f"other_application_{activity.application_id}"] += 1
                                activities[f"other_name_{activity.name}"] += 1
                                activities[f"other_state_{activity.state}"] += 1
                                activities[f"other_type_{activity.type}"] += 1
                        else:
                            activities["none"] += 1

                    categories = 0
                    text_channels = 0
                    voice_channels = 0

                    for channel in channels:
                        if isinstance(channel, CategoryChannel):
                            categories += 1

                        elif isinstance(channel, VoiceChannel):
                            voice_channels += 1

                        else:
                            text_channels += 1

                    data = {
                        "guild_id": guild.id,

                        "members_bot": bots,
                        "members_count": guild.member_count,

                        "members_dnd": dnd,
                        "members_idle": idle,
                        "members_online": online,
                        "members_offline": offline,

                        "channels_count": len(channels),

                        "channels_category": categories,
                        "channels_text": text_channels,
                        "channels_voice": voice_channels,
                    }

                    for key, value in activities.items():
                        data[f"activity_{key}"] = value

                    await self.cog.submit_point(
                        now, "guild_stats",
                        tags=data,
                        fields=data
                    )

                    await self.cog.submit_points(*spotify_points, measurement="spotify")
            except Exception:
                log.exception("Failed to run collection task")
            else:
                log.info(f"Collection task run: {len(self.bot.guilds)} guilds")
