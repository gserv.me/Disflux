from cerberus import Validator

POINT_SCHEMA = {
    "fields": {"validator": "fields"},
    "measurement": {"type": "string"},
    "tags": {"validator": "tags"},
    "time": {"type": "datetime"}
}


class PointValidator(Validator):
    def _validator_tags(self, field, value):
        if not isinstance(value, dict):
            self._error(field, "Must be a dictionary")

        for key, value in value.items():
            if not isinstance(key, str) or not isinstance(key, str):
                self._error(field, "All keys and values must be strings")

    def _validator_fields(self, field, value):
        if not isinstance(value, dict):
            self._error(field, "Must be a dictionary")

        for key, value in value.items():
            if not isinstance(key, str):
                self._error(field, "All keys must be strings")


POINT_VALIDATOR = PointValidator(POINT_SCHEMA)
