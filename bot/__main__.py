import logging

from discord.ext.commands import Bot, when_mentioned_or

from bot.config import Config

logging.basicConfig(
    format="%(asctime)s Disflux Bot: | %(name)30s | %(levelname)8s | %(message)s",
    datefmt="%b %d %H:%M:%S",
    level=logging.INFO
)

logging.getLogger("discord.client").setLevel(logging.ERROR)
logging.getLogger("discord.gateway").setLevel(logging.ERROR)
logging.getLogger("discord.state").setLevel(logging.ERROR)
logging.getLogger("discord.http").setLevel(logging.ERROR)
logging.getLogger("websockets.protocol").setLevel(logging.ERROR)

config = Config()
log = logging.getLogger(__name__)

bot = Bot(
    command_prefix=when_mentioned_or("!!!"),
    # activity=Game(name="Commands: !help"),
    case_insensitive=True
)

bot.config = config

bot.load_extension("bot.cogs.metrics")
bot.run(config.bot_token)
