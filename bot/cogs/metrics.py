import asyncio
import logging
from datetime import datetime
from discord.abc import Messageable, PrivateChannel
from typing import Any, Dict

from aioinflux import InfluxDBClient
from emoji import demojize
from discord import (
    Message, RawReactionActionEvent, PartialEmoji, User, RawMessageDeleteEvent,
    TextChannel, RawBulkMessageDeleteEvent, RawMessageUpdateEvent)
from discord.ext.commands import Bot

from bot.collection_task import PeriodicStatsCollector
from bot.validation import POINT_VALIDATOR

log = logging.getLogger(__name__)


class Metrics:
    def __init__(self, bot: Bot):
        self.bot = bot
        self.config = bot.config  #: type: Config
        self.influx = InfluxDBClient(
            db=self.config.influx_database,
            host=self.config.influx_host, port=self.config.influx_port,
            username=self.config.influx_username,
            password=self.config.influx_password
        )

        self.collection_task = PeriodicStatsCollector(self.bot, self, 60)
        asyncio.ensure_future(self.influx.create_database())

    def __unload(self):
        asyncio.run_coroutine_threadsafe(self.collection_task.stop(), loop=self.bot.loop)

    async def submit_point(self, time: datetime, measurement: str, tags: Dict[str, str], fields: Dict[str, Any]):
        point = {
            "time": time,
            "measurement": measurement,
            "tags": {k: str(v) for k, v in tags.items()},
            "fields": fields
        }

        await self.submit_points(point)

    async def submit_points(self, *points: Dict[str, Any], time: datetime = None, measurement: str = None):
        for point in points:
            point["time"] = point.get("time", time)
            point["measurement"] = point.get("measurement", measurement)

            if not POINT_VALIDATOR.validate(point):
                error = f"Failed to validate point: {point}\n\n"

                for key, value in POINT_VALIDATOR.errors.items():
                    messages = ", ".join(value)
                    error += f"{key}: {messages}\n"

                raise ValueError(error)

            point["fields"]["value"] = point["fields"].get("value", 1)

        await self.influx.write(points)

    async def on_ready(self):
        log.info(f"Connected; {len(self.bot.guilds)} guilds")

        await self.collection_task.start()

    async def on_socket_raw_receive(self, _msg):
        await self.submit_point(datetime.utcnow(), "socket_recv", {}, {})

    async def on_socket_raw_send(self, _msg):
        await self.submit_point(datetime.utcnow(), "socket_send", {}, {})

    async def on_typing(self, _channel: Messageable, _user: User, when: datetime):
        if not isinstance(_channel, TextChannel):
            return  # This is a DM, so it's not relevant to us

        await self.submit_point(when, "typing", {}, {})

    async def on_message(self, message: Message):
        if not message.guild:
            return  # This is a DM, so it's not relevant to us

        data = {
            "activity_type": None,
            "activity_party_id": None,
            "application_id": None,
            "application_name": None,

            "author_id": message.author.id,
            "channel_id": message.channel.id,
            "guild_id": message.guild.id,
            "message_id": message.id,

            "is_from_bot": message.author.bot,
            "is_tts": message.tts,

            "has_everyone_mention": message.mention_everyone,

            "num_attachments": len(message.attachments),
            "num_channel_mentions": len(message.channel_mentions),
            "num_role_mentions": len(message.role_mentions),
            "num_user_mentions": len(message.mentions),
        }

        if message.activity:
            data["activity_type"] = message.activity.get("type")
            data["activity_party_id"] = message.activity.get("party_id")

        if message.application:
            data["application_id"] = message.application.get("id")
            data["application_name"] = message.application.get("name")

        await self.submit_point(
            message.created_at, "message_sent",
            tags=data,
            fields=data
        )

    async def on_raw_message_delete(self, event: RawMessageDeleteEvent):
        if not event.guild_id:
            return  # This is a DM, so it's not relevant to us

        data = {
            "channel_id": event.channel_id,
            "guild_id": event.guild_id,
            "message_id": event.message_id,
        }

        await self.submit_point(
            datetime.utcnow(), "message_deleted",
            tags=data,
            fields=data
        )

    async def on_raw_bulk_message_delete(self, event: RawBulkMessageDeleteEvent):
        if not event.guild_id:
            return  # This is a DM, so it's not relevant to us

        for message_id in event.message_ids:
            data = {
                "channel_id": event.channel_id,
                "guild_id": event.guild_id,
                "message_id": message_id,
            }

            await self.submit_point(
                datetime.utcnow(), "message_deleted",
                tags=data,
                fields=data
            )

    async def on_raw_message_edit(self, event: RawMessageUpdateEvent):
        data = event.data
        channel = self.bot.get_channel(data.get("channel_id"))

        if not channel or isinstance(channel, PrivateChannel):
            return  # This is a DM, so it's not relevant to us

        if "content" not in data:
            return  # Not a contentful edit; it's a pin/unpin/embed from discord

        after = await channel.get_message(event.message_id)  # type: Message

        data = {
            "activity_type": None,
            "activity_party_id": None,
            "application_id": None,
            "application_name": None,

            "author_id": after.author.id,
            "channel_id": after.channel.id,
            "guild_id": after.guild.id,
            "message_id": after.id,

            "is_from_bot": after.author.bot,
            "is_tts": after.tts,

            "has_everyone_mention": after.mention_everyone,

            "num_attachments": len(after.attachments),
            "num_channel_mentions": len(after.channel_mentions),
            "num_role_mentions": len(after.role_mentions),
            "num_user_mentions": len(after.mentions),
        }

        if after.activity:
            data["activity_type"] = after.activity.get("type")
            data["activity_party_id"] = after.activity.get("party_id")

        if after.application:
            data["application_id"] = after.application.get("id")
            data["application_name"] = after.application.get("name")

        await self.submit_point(
            datetime.utcnow(), "message_edited",
            tags=data,
            fields=data
        )

    async def on_raw_reaction_add(self, event: RawReactionActionEvent):
        emoji = event.emoji  # type: PartialEmoji

        if emoji.is_custom_emoji():
            name = f":{emoji.name}:"
            is_custom = True
        else:
            name = demojize(emoji.name)
            is_custom = False

        data = {
            "author_id": event.user_id,
            "channel_id": event.channel_id,
            "emoji_id": emoji.id,
            "guild_id": event.guild_id,
            "message_id": event.message_id,

            "is_custom": is_custom,

            "name": name,
        }

        await self.submit_point(
            datetime.utcnow(), "reaction_added",
            tags=data,
            fields=data
        )

    async def on_raw_reaction_remove(self, event: RawReactionActionEvent):
        emoji = event.emoji  # type: PartialEmoji

        if emoji.is_custom_emoji():
            name = f":{emoji.name}:"
            is_custom = True
        else:
            name = demojize(emoji.name)
            is_custom = False

        data = {
            "author_id": event.user_id,
            "channel_id": event.channel_id,
            "emoji_id": emoji.id,
            "guild_id": event.guild_id,
            "message_id": event.message_id,

            "is_custom": is_custom,

            "name": name,
        }

        await self.submit_point(
            datetime.utcnow(), "reaction_removed",
            tags=data,
            fields=data
        )


def setup(bot: Bot):
    bot.add_cog(Metrics(bot))
    log.info("Cog loaded: Metrics")
